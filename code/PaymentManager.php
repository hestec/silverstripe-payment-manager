<?php
class PaymentManager extends Payment {

    private static $default_sort = 'ID DESC';

    private static $summary_fields = array(
        'ID',
        'Created.Nice24',
        'PaidBy.Name',
        'PaidBy.Email',
        'Amount',
        'Method',
        'Status'
    );

    function updateFieldLabels(&$labels) {
        parent::updateFieldLabels($labels);
        $labels['PaidBy.Name'] = _t("PaymentManagerAdmin.PAIDBY", "Paid by");
        $labels['Created.Nice24'] = _t("PaymentManagerAdmin.DATE_TIME", "Date & time");
        $labels['PaidBy.Email'] = _t("PaymentManagerAdmin.EMAIL", "Email address");
        $labels['Amount'] = _t("PaymentManagerAdmin.AMOUNT", "Amount");
        $labels['Status'] = _t("PaymentManagerAdmin.STATUS", "Status");
        $labels['Method'] = _t("PaymentManagerAdmin.METHOD", "Method");
        $labels['Payments'] = _t("PaymentManagerAdmin.PAYMENTS", "Payments");
    }

    public function updateCMSFields(FieldList $fields) {


        $fields->removeByName(array(
            'Main',
            'Errors'
        ));

        $supported_methods = PaymentProcessor::get_supported_methods();
        $gateways = array();
        foreach ($supported_methods as $methodName) {
            $methodConfig = PaymentFactory::get_factory_config($methodName);
            $gateways[$methodName] = $methodConfig['title'];
        }

        $paymentidfield = ReadonlyField::create('ID', _t("PaymentManagerAdmin.PAYMENTID", "Payment id"));
        $methodfield = DropdownField::create('Method', _t("PaymentManagerAdmin.METHOD", "Method"), $gateways);
        $amountfield = ReadonlyField::create('AmountField', _t("PaymentManagerAdmin.AMOUNT", "Amount"), $this->owner->Amount->getCurrency()." ".number_format($this->owner->Amount->getAmount(), 2, '.', ''));
        $paidbynamefield = ReadonlyField::create('PaidBy.Email', _t("PaymentManagerAdmin.PAIDBY", "Paid by"), $this->owner->PaidBy()->Email);
        $statusfield = DropdownField::create('Status', _t("PaymentManagerAdmin.STATUS", "Status"), singleton('Payment')->dbObject('Status')->enumValues());
        $changeinfofield = LiteralField::create('changeinfo', _t("PaymentManagerAdmin.CHANGEINFO", "If necessary here you can change the Method and Status manually."));

        $ErrorsGridField = new GridField(
            'Errors',
            _t('PaymentManagerAdmin.ERRORS','Errors'),
            $this->owner->Errors(),
            GridFieldConfig::create()
                ->addComponent(new GridFieldToolbarHeader())
                ->addComponent(new GridFieldSortableHeader())
                ->addComponent(new GridFieldDataColumns())
                ->addComponent(new GridFieldPaginator(50))
        );

        $fields->addFieldsToTab('Root.' . _t("PaymentManagerAdmin.PAYMENTS", "Payments"), array(
            $paymentidfield,
            $paidbynamefield,
            $amountfield,
            $changeinfofield,
            $methodfield,
            $statusfield,
            $ErrorsGridField
        ));

    }

    public static function getPaymentMethods($amount = 0){

        $supported_methods = PaymentProcessor::get_supported_methods();

        $count_methods = count($supported_methods);

        // make a IN string for mysql to check if the method is (still) in the yaml config file
        $wherein = "'";
        foreach ($supported_methods as $key=>$methodname) {
            if ($count_methods == $key+1){
                $wherein .= $methodname."'";
            }else{
                $wherein .= $methodname."','";
            }
        }

        $output = PaymentManagerMethod::get()->filter(array('Enabled' => 1))->where("Method IN (".$wherein.") AND MinAmountAmount <= ".$amount." AND (MaxAmountAmount >= ".$amount." OR MaxAmountAmount = 0)")->sort('SortOrder');

        return $output;

    }

}