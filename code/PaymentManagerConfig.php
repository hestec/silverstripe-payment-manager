<?php
class PaymentManagerConfig extends DataObject {

    private static $singular_name = 'Settings';
    private static $plural_name = 'Settings';

    private static $db = array(
        'TestMode' => 'Boolean',
        'BaseCurrency' => 'Varchar(3)'
    );

    private static $defaults = array(
        'BaseCurrency' => 'EUR'
    );

    private static $has_many = array(
        'TestIPAdresses' => 'PaymentManagerTestIP',
        'PaymentManagerMethods' => 'PaymentManagerMethod'
    );

    public static function current_paymentmanagerconfig() {

        return PaymentManagerConfig::get()->First();

    }

    public function requireDefaultRecords() {

        parent::requireDefaultRecords();

        if(!self::current_paymentmanagerconfig()) {
            $shopConfig = new PaymentManagerConfig();
            $shopConfig->write();
            DB::alteration_message('Added default PaymentManagerConfig', 'created');
        }
    }

    public function getSummaryTitle(){

        return _t("PaymentManagerAdmin.CLICK_TO_OPEN_CONFIG","Click to open the settings object");

    }

    private static $summary_fields = array(
        'getSummaryTitle' => '',
    );

    public function getCMSFields() {

        $PaymentManagerMethodsGridField = new GridField(
            'PaymentManagerMethods',
            _t('PaymentManagerAdmin.MANAGED_PAYMENT_METHODS','Managed payment methods'),
            $this->PaymentManagerMethods(),
            GridFieldConfig::create()
                ->addComponent(new GridFieldToolbarHeader())
                ->addComponent(new GridFieldAddNewButton("toolbar-header-right"))
                ->addComponent(new GridFieldSortableHeader())
                ->addComponent(new GridFieldDataColumns())
                ->addComponent(new GridFieldPaginator(50))
                ->addComponent(new GridFieldEditButton())
                ->addComponent(new GridFieldDeleteAction())
                ->addComponent(new GridFieldDetailForm())
                ->addComponent(new GridFieldFilterHeader())
                ->addComponent(new GridFieldOrderableRows('SortOrder'))
        );

        $PaymentManagerTestIPAdressesGridField = new GridField(
            'TestIPAdresses',
            _t('PaymentManagerAdmin.TESTIPADRESSES','Test ip adresses'),
            $this->TestIPAdresses(),
            GridFieldConfig::create()
                ->addComponent(new GridFieldToolbarHeader())
                ->addComponent(new GridFieldAddNewButton("toolbar-header-right"))
                ->addComponent(new GridFieldSortableHeader())
                ->addComponent(new GridFieldDataColumns())
                ->addComponent(new GridFieldPaginator(50))
                ->addComponent(new GridFieldEditButton())
                ->addComponent(new GridFieldDeleteAction())
                ->addComponent(new GridFieldDetailForm())
                ->addComponent(new GridFieldFilterHeader())
        //->addComponent(new GridFieldSortableRows('SortOrder'))
        );

        //}

        $methodsheaderfield = LiteralField::create('methodsheader', "<h3>"._t("PaymentManagerAdmin.MANAGED_PAYMENT_METHODS", "Managed payment methods")."</h3>");
        //$methodsinfofield = LiteralField::create('methodsinfo', _t("PaymentManagerAdmin.METHODSINFO", "Here you can (temporary) enable/disable the available payment methods."));

        $basecurrencyfield = TextField::create('BaseCurrency', _t('PaymentManagerAdmin.BASECURRENCY', 'Base currency'));
        $basecurrencyfield->setDescription(_t('PaymentManagerAdmin.BASECURRENCY_DESCRIPTION', 'The 3 character currency code.'));

        $testmodeheaderfield = LiteralField::create('testmodeheader', "<h3>"._t("PaymentManagerAdmin.TESTMODEHEADERFIELD","Test mode")."</h3>");
        $testmodefield = CheckboxField::create('TestMode', _t("PaymentManagerAdmin.TESTMODE", "Test mode"));
        $testmodefield->setDescription(_t("PaymentManagerAdmin.TESTMODE_DESCRIPTION","The test mode will only affect the ip adresses under the tab Test ip adresses."));


        $Fields = new FieldList(
            $basecurrencyfield,
            $methodsheaderfield,
            $PaymentManagerMethodsGridField,
            $testmodeheaderfield,
            $testmodefield,
            $PaymentManagerTestIPAdressesGridField
        );


        $this->extend('updateCMSFields', $Fields);
        return $Fields;

    }

    public function validate() {
        $result = parent::validate();
        if(strlen($this->BaseCurrency) != 3) {
            $result->error(_t('PaymentManagerAdmin.INVALID_BASECURRENCY','This is not a valid currency code'));
        }
        return $result;
    }

    public function canView($member=null) {
        return Permission::check('ADMIN') || Permission::check('CMS_ACCESS_PaymentManagerAdmin') || Permission::check('CMS_ACCESS_LeftAndMain') ;
    }

    public function canEdit($member=null) {
        return Permission::check('ADMIN') || Permission::check('CMS_ACCESS_PaymentManagerAdmin') || Permission::check('CMS_ACCESS_LeftAndMain') ;
    }

    public function canCreate($member=null) {
        //return Permission::check('ADMIN') || Permission::check('CMS_ACCESS_SisowPaymentAdmin') || Permission::check('CMS_ACCESS_LeftAndMain') ;
    }

    public function canDelete($member=null) {
        //return Permission::check('ADMIN') || Permission::check('CMS_ACCESS_SisowPaymentAdmin') || Permission::check('CMS_ACCESS_LeftAndMain') ;
    }

}