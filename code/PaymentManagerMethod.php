<?php

class PaymentManagerMethod extends DataObject
{

    private static $singular_name = 'Payment Method';
    private static $plural_name = 'Payment Methods';

    private static $db = array(
        'Method' => 'Varchar(50)',
        'MethodTitle' => 'Varchar(50)',
        'MethodDescription' => 'Text',
        'MinAmount' => 'Money',
        'MaxAmount' => 'Money',
        'ExtraAmount' => 'Money',
        'ExtraPercent' => 'Decimal(6,2)',
        'Enabled' => 'Boolean(1)',
        'SortOrder' => 'Int'
    );

    private static $summary_fields = array(
        'Method',
        'MethodTitle',
        'MinAmount',
        'MaxAmount',
        'ExtraAmount',
        'ExtraPercent',
        'Enabled.Nice'
    );

    private static $has_one = array(
        'PaymentManagerConfig' => 'PaymentManagerConfig'
    );

    function fieldLabels($includerelations = true) {
        $labels = parent::fieldLabels($includerelations);

        $labels['Method'] = _t("PaymentManagerAdmin.METHOD", "Payment method");
        $labels['MethodTitle'] = _t("PaymentManagerAdmin.METHODTITLE", "Web title");
        $labels['MinAmount'] = _t("PaymentManagerAdmin.MINAMOUNT", "Minimal amount");
        $labels['MaxAmount'] = _t("PaymentManagerAdmin.MAXAMOUNT", "Maximal amount");
        $labels['ExtraAmount'] = _t("PaymentManagerAdmin.EXTRAAMOUNT", "Surcharge");
        $labels['ExtraPercent'] = _t("PaymentManagerAdmin.EXTRAPERCENT", "Surcharge percentage");
        $labels['Enabled.Nice'] = _t("PaymentManagerAdmin.ENABLED", "Enabled");

        return $labels;
    }

    public function getCMSFields() {

        // Field labels
        $l = $this->fieldLabels();

        $supported_methods = PaymentProcessor::get_supported_methods();
        $gateways = array();
        foreach ($supported_methods as $methodName) {
            // check if method is already added
            if (PaymentManagerMethod::get()->filter('Method', $methodName)->count() == 0){

                $methodConfig = PaymentFactory::get_factory_config($methodName);
                $gateways[$methodName] = $methodConfig['title'];

            }
        }

        $basecurr = PaymentManagerConfig::get()->byID(1)->BaseCurrency;

        if (!$this->ID){
            $methodfield = DropdownField::create('Method', _t('PaymentManagerAdmin.METHOD', "Payment method"), $gateways);
            $methodfieldro = HiddenField::create('Methodro', '');
        }else{
            $methodfield = HiddenField::create('Method', _t('PaymentManagerAdmin.METHOD', "Payment method"), $this->Method);
            $methodfieldro = ReadonlyField::create('Methodro', _t('PaymentManagerAdmin.METHOD', "Payment method"), $this->Method);
        }

        $methodtitlefield = TextField::create('MethodTitle', _t('PaymentManagerAdmin.METHODTITLE', "Web title"));
        $methodtitlefield->setDescription(_t('PaymentManagerAdmin.METHODTITLE_DESCRIPTION', "Optional, if empty the default name for this payment method will be displayed."));
        $methoddescriptionfield = TextareaField::create('MethodDescription', _t('PaymentManagerAdmin.METHODDESCRIPTION', "Extra description"));
        $methoddescriptionfield->setDescription(_t('PaymentManagerAdmin.METHODDESCRIPTION_DESCRIPTION', "Optional."));
        $minamount = MoneyField::create('MinAmount', _t('PaymentManagerAdmin.MINAMOUNT', "Minimal amount"));
        $minamount->setAllowedCurrencies(array($basecurr));
        $minamount->setDescription(_t('PaymentManagerAdmin.MINAMOUNT_DESCRIPTION', "Minimal amount to display this payment method, leave empty if there is no minimum required."));
        $maxamount = MoneyField::create('MaxAmount', _t('PaymentManagerAdmin.MAXAMOUNT', "Maximal amount"));
        $maxamount->setAllowedCurrencies(array($basecurr));
        $maxamount->setDescription(_t('PaymentManagerAdmin.MAXAMOUNT_DESCRIPTION', "Maximum amount to display this payment method, leave empty if there is no maximum required."));
        $extraamount = MoneyField::create('ExtraAmount', _t('PaymentManagerAdmin.EXTRAAMOUNT', "Extra surcharge"));
        $extraamount->setAllowedCurrencies(array($basecurr));
        $extraamount->setDescription(_t('PaymentManagerAdmin.EXTRAAMOUNT_DESCRIPTION', "Extra surcharge for this payment method, leave empty if there is no extra surcharge."));
        $extrapercent = TextField::create('ExtraPercent', _t('PaymentManagerAdmin.EXTRAPERCENT', "Extra surcharge percentage"));
        $extrapercent->setDescription(_t('PaymentManagerAdmin.EXTRAPERCENT_DESCRIPTION', "Extra surcharge in percentage for this payment method, leave empty if there is no extra surcharge in percentage."));
        $enabledfield = CheckboxField::create('Enabled', _t('PaymentManagerAdmin.ENABLED', "Enabled"));

        return new FieldList(
            $methodfield,
            $methodfieldro,
            $methodtitlefield,
            $methoddescriptionfield,
            $minamount,
            $maxamount,
            $extraamount,
            $extrapercent,
            $enabledfield
        );

    }

    public function getCMSValidator() {
        return new RequiredFields(array(
            'Method'
        ));
    }

    public function onBeforeWrite() {

        if (empty($this->MethodTitle)){

            $methodConfig = PaymentFactory::get_factory_config($this->Method);
            $this->MethodTitle = $methodConfig['title'];
        }


        parent::onBeforeWrite();

    }

    public function canView($member=null) {
        return Permission::check('ADMIN') || Permission::check('CMS_ACCESS_PaymentManagerAdmin') || Permission::check('CMS_ACCESS_LeftAndMain') ;
    }

    public function canEdit($member=null) {
        return Permission::check('ADMIN') || Permission::check('CMS_ACCESS_PaymentManagerAdmin') || Permission::check('CMS_ACCESS_LeftAndMain') ;
    }

    public function canCreate($member=null) {
        return Permission::check('ADMIN') || Permission::check('CMS_ACCESS_PaymentManagerAdmin') || Permission::check('CMS_ACCESS_LeftAndMain') ;
    }

    public function canDelete($member=null) {
        return Permission::check('ADMIN') || Permission::check('CMS_ACCESS_PaymentManagerAdmin') || Permission::check('CMS_ACCESS_LeftAndMain') ;
    }

}