<?php

class PaymentManagerError extends DataExtension {

    private static $summary_fields = array(
        'ID',
        'ErrorCode',
        'ErrorMessage'
    );

    function updateFieldLabels(&$labels) {
        parent::updateFieldLabels($labels);
        $labels['ErrorCode'] = _t("PaymentManagerAdmin.ERRORCODE", "Error code");
        $labels['ErrorMessage'] = _t("PaymentManagerAdmin.ERRORMESSAGE", "Error message");
    }

}
