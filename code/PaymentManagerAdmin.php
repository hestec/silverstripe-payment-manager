<?php
class PaymentManagerAdmin extends ModelAdmin {

    //private static $managed_models = array('Service' => array('title' => 'Services'),'ServiceEmployee' => array('title' => 'ServiceEmployees'));

    private static $managed_models = array(
        'Payment',
        'PaymentManagerConfig'
    );

    // disable the importer
    private static $model_importers = array();

    // Linked as /admin/slides/
    static $url_segment = 'payment-manager';

    // title in cms navigation
    static $menu_title = 'Payment';

    // menu icon
    static $menu_icon = 'payment-manager/images/icons/paymentmanager_icon.png';

    function getEditForm($id = null, $fields = null) {
        $form=parent::getEditForm($id, $fields);

        // get gridfield
        $gridfield = $form->Fields()
                        ->dataFieldByName($this->sanitiseClassName($this->modelClass));

        if($this->modelClass == 'Payment') {


            $gridfieldConfig = $gridfield->getConfig();

            $gridfieldConfig
                ->removeComponentsByType('GridFieldDeleteAction')
                ->removeComponentsByType('GridFieldAddNewButton');

        }

        if($this->modelClass == 'PaymentManagerConfig') {


            $gridfieldConfig = $gridfield->getConfig();

            $gridfieldConfig
                ->removeComponentsByType('GridFieldDeleteAction')
                ->removeComponentsByType('GridFieldAddNewButton')
                ->removeComponentsByType('GridFieldExportButton')
                ->removeComponentsByType('GridFieldPrintButton');

        }

        return $form;
    }

}
