# SilverStripe Payment Manager #

### Overview ###

Addon for the SilverStripe Payment Module 1.0 (https://github.com/silverstripe-labs/silverstripe-payment/tree/1.0).

### Features ###

* Adds a modeladmin module to the cms.
* Summary of the payments.
* Summary of payment errors.
* Update payment Status and Method manually.
* Manage the payment methods.
* Enable/disable payment method in the cms.
* Add minimum and/or maximum amount to every payment method (for example: display PayPal only for amount below $1000.
* Add surcharge to payment method in percent and/or fixed amount.
* Change sort order of the methods in the cms.
* Set a custom display name for the payment method.
* Enable test modus based on ip adresses.

##### Disclaimer #####

De working of the features is dependent of the payment method module(s) as well. This module in in the first place written for the Payment-Sisow module (https://bitbucket.org/hestec/silverstripe-payment-sisow) but should work for the other payment method modules as well.

### Version ###

Using Semantic Versioning.

### Requirements ###

* SilverStripe > 3.1, tested untill 3.3.1.
* SilverStripe Payment Module 1.0.
* SilverStripe Grid Field Extensions Module 1.*

### Installation ###

composer require hestec/silverstripe-payment-manager 1.0.*

In the frontend you have to get the payment methods in another way, see the example page type in the folder /examples.

Config of the payment methods in the same way, see https://github.com/silverstripe-labs/silverstripe-payment/tree/1.0.

### Usage ###

When you have configured the payment methods in the yaml files you can (have to) add this methods in the manager as well under the tab settings.

### Todo ###

In the example the payment does nothing with the surcharge which you can add to the payment methods yet.