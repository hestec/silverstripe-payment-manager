<?php

class ExamplePaymentPage extends Page {

}

class ExamplePaymentPage_Controller extends Page_Controller {
    
    private static $allowed_actions = array (
        'PaymentForm',
        'Pay',
        'payfinished'
    );


    public function PaymentForm() {

        // if payment is iDEAL
        if ($payarray = Session::get('PayFormArray')){
            //return $payarray['Payment'];

            //get ideal banks
            $banks = new SisowRequest();
            $source = $banks->getIdealBanks();


            $idealbankfield = DropdownField::create('IssuerID', 'bankkeuze', $source);
            $paymentfield = HiddenField::create('Payment', '', $payarray['Payment']);
            $amountfield = HiddenField::create('Amount', '', $payarray['Amount']);

            Session::clear('PayFormArray');


            $form = Form::create(
                $this->owner,
                __FUNCTION__,
                FieldList::create(
                    new LiteralField('Info', "info"),
                    $amountfield,
                    $paymentfield,
                    $idealbankfield
                ),
                FieldList::create(
                    new FormAction('Pay', _t('General.BETALEN', "PAY"))
                )//,
            //RequiredFields::create('UnitsOrPay')*/
            );

            return $form;

        }else {

            // amount to use in this test form, in real situation this comes from your shop/checkout
            $amount = 1.23;

            $methods = PaymentManager::getPaymentMethods($amount);

            $gateways = array();
            foreach ($methods as $method) {
                $gateways[$method->Method] = "<img src=\"/payment-manager/images/" . $method->Method . ".png\"> " . $method->MethodTitle;
            }

            $amountfield = HiddenField::create('Amount', 'Amount', $amount);
            $paymentfield = OptionsetField::create('Payment', _t('General.CHOOSE_YOUR_PAY_METHOD', "Choose your pay method"), $gateways)->setHasEmptyDefault(1);
            $finalinfopaymentfield = LiteralField::create('finalinfopayment', '<p>' . _t('General.CLICK_BUTTON_PAY', "Click the Pay button to continue.") . '</p>');

            $form = Form::create(
                $this->owner,
                __FUNCTION__,
                FieldList::create(
                    $amountfield,
                    $paymentfield,
                    $finalinfopaymentfield
                ),
                FieldList::create(
                    new FormAction('Pay', _t('General.PAY', "PAY"))
                ),
                RequiredFields::create(array('Payment'))
            );

            return $form;

        }

    }

    public function Pay($data, $form) {

        if (isset($data['Payment'])) {

            if ($data['Payment'] == "Sisow_iDEAL" && !is_numeric($data['IssuerID'])){

                Session::set('PayFormArray', $data);

                return $this->owner->redirectBack();

            }else {

                if (isset($data['IssuerID'])) {
                    $issuerid = $data['IssuerID'];
                } else {
                    $issuerid = '';
                }

                $paymentData = array(
                    'Amount' => $data['Amount'],
                    'Currency' => 'EUR',
                    'PaymentMethod' => $data['Payment'],
                    'IssuerID' => $issuerid,
                    'Reference' => 1, // 1 is just for example, in real situation this comes from your shop/checkout
                    'Description' => "TEST",
                    'PaidBy' => 1, // 1 is just for example, in real situation this comes from your shop/checkout
                );

                $this->process($paymentData);

            }

        }else {
            $form->sessionMessage(_t('General.NO_PAY_METHOD_SELECTED', "You did not select the method of payment. Select a method of payment."),'bad');
            return $this->owner->redirectBack();
        }

    }

    public function process($data) {

        //Check payment type
        try {
            $paymentMethod = Convert::raw2sql($data['PaymentMethod']);
            $paymentProcessor = PaymentFactory::factory($paymentMethod);
        }
        catch (Exception $e) {
            Debug::friendlyError(
                403,
                _t('CheckoutPage.NOT_VALID_METHOD',"Sorry, that is not a valid payment method."),
                _t('CheckoutPage.TRY_AGAIN',"Please go back and try again.")
            );
            return;
        }

        try {

            $paymentData = array(
                'Amount' => number_format($data['Amount'], 2, '.', ''),
                'Currency' => $data['Currency'],
                'Reference' => $data['Reference'],
                'IssuerID' => $data['IssuerID'],
                'Description' => $data['Description']
            );
            $paymentProcessor->payment->OrderID = '';
            $paymentProcessor->payment->PaidByID = $data['PaidBy'];

            $paymentProcessor->setRedirectURL($this->owner->Link('payfinished'));
            $paymentProcessor->capture($paymentData);
        }
        catch (Exception $e) {

            //This is where we catch gateway validation or gateway unreachable errors
            $result = $paymentProcessor->gateway->getValidationResult();
            //$payment = $paymentProcessor->payment;

            SS_Log::log(new Exception(print_r($result->message(), true)), SS_Log::NOTICE);
            SS_Log::log(new Exception(print_r($e->getMessage(), true)), SS_Log::NOTICE);

            $this->redirect($this->owner->Link(''));
        }

    }

    public function payfinished() {

        /*$payment = Payment::get()->byID(Session::get('PaymentID'));

        switch($payment->Status){
            case "Success":
                $finishcontent = _t('General.TRANSACTION_STATUS_SUCCESS', "Your payment has been processed successfully.");
                break;
            default:
                $finishcontent = _t('General.TRANSACTION_STATUS_UNKNOWN', "The transaction has failed for unknown reasons.");
                break;
        }*/

        return array(
            'Content' => '<p>Your payment has been processed!</p>'
        );

        //Session::clear('PaymentID');

    }
    
}